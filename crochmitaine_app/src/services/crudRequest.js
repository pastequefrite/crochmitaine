import axios from 'axios';

export const executeGetRequest = async (apiEndPoint) => {
    return await axios.get(apiEndPoint);
};


export const executePostRequest = async (apiEndPoint, data) => {
    return await axios.post(apiEndPoint, data);
};
