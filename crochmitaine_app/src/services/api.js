import config from './apiConfig';
import {executeGetRequest, executePostRequest} from './crudRequest';


export const getProductDetails = async (id) => {
    return await executeGetRequest(config.url + '/products/' + id + '/');
};

export const getProducts = async (params) => {
    return await executeGetRequest(config.url + '/products/?' + _objToQueryString(params));
};

export const getCategories = async () => {
    return await executeGetRequest(config.url + '/categories/')
};

export const login = async (credentials) => {
    return await executePostRequest(config.url + '/auth/login/', credentials);
};

export const register = async (data) => {
    return await executePostRequest(config.url + '/auth/registration/', data);
};


const _objToQueryString = (params) => {
    return Object.keys(params).map((key) => {
        if(params[key]) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
        }
    }).join('&');
};
