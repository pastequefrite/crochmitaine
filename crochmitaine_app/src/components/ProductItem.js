import React from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity, Dimensions} from 'react-native';
import PropTypes from 'prop-types';

import theme from '../styles/theme.style'

class ProductItem extends React.Component {

    _displayImage = () => {
        if (this.props.product.image) {
            return <Image style={[styles.image, this.props.imageStyle]} source={{uri: this.props.product.image}}/>
        }
    };

    render() {
        return (
            <TouchableOpacity
                style={[styles.container, {height: Dimensions.get('window').width / this.props.numColumns, flex: 1/ this.props.numColumns}]}
                onPress={() => this.props.displayProductDetailsForProduct(this.props.product.id)}>
                <View style={[styles.imageContainer, this.props.imageStyle]}>
                    {this._displayImage()}
                </View>
                <View style={[styles.textContainer, {display: this.props.displayText}]}>
                    <Text style={styles.title}>{this.props.product.title}</Text>
                    <Text style={styles.price}>{this.props.product.price_ttc} €</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

ProductItem.propTypes = {
    product: PropTypes.object.isRequired,
    numColumns: PropTypes.number,
    displayProductDetailsForProduct: PropTypes.func.isRequired,
    imageStyle: PropTypes.object,
    displayText: PropTypes.string,
};

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: theme.DEFAULT_PADDING,
        paddingVertical: 5,
        justifyContent: 'center',
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
        borderRadius: 10,
    },
    imageContainer: {
        backgroundColor: theme.SECONDARY_COLOR,
        flex: 1,
        borderRadius: 10,
    },
    textContainer: {
        alignSelf: 'center',
        padding: 2,
    },
    title: {
        // fontSize: theme.FONT_SIZE_LARGE,
        textAlign: 'center',
    },
    price: {
        textAlign: 'center',
        color: theme.SECONDARY_COLOR,
    }
});

export default ProductItem
