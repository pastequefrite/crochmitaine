import React from 'react';
import {Modal} from 'react-native';

import ImageViewer from 'react-native-image-zoom-viewer';

import PropTypes from 'prop-types';

class ProductImageModal extends React.Component {
    render() {
        return (
            <Modal animationType="slide"
                   transparent={false}
                   visible={this.props.visible}
            >
                <ImageViewer imageUrls={this.props.images} onSwipeDown={this.props.toggleModal} enableSwipeDown/>
            </Modal>
        )
    }
}

ProductImageModal.propTypes = {
    visible: PropTypes.bool.isRequired,
    toggleModal: PropTypes.func.isRequired,
    images: PropTypes.array.isRequired,
};


export default ProductImageModal
