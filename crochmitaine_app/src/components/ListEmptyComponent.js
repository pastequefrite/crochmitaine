import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

import theme from '../styles/theme.style'
import {FontAwesomeIcon} from "@fortawesome/react-native-fontawesome";
import {faTimes} from "@fortawesome/free-solid-svg-icons";

class ListEmptyComponent extends React.Component {

    render() {
        if (this.props.data) {
            return null;
        }
        else {
            return (
                <View style={styles.mainContainer}>
                    <View style={styles.subContainer}>
                        <FontAwesomeIcon icon={faTimes} size={64} color={theme.SECONDARY_COLOR}/>
                        <Text style={styles.title}>Aucune donnée trouvée</Text>
                    </View>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        padding: 10,
    },
    subContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: theme.FONT_SIZE_LARGE,
        paddingVertical: 15,
        textAlign: 'center',
    },
    button: {
        alignSelf: 'center',
    }
});


export default ListEmptyComponent
