import React from 'react';
import {StyleSheet, KeyboardAvoidingView, View, Button as Btn, Image} from 'react-native';
import {Button, Input} from 'react-native-elements';

import theme from '../styles/theme.style';
import Logo from '../assets/icons/logo.png';

class RegisterScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            first_name: '',
            last_name: '',
            password: '',
            password2: '',
            errors: [],
        }
    }

    _register = () => {
        alert("register");
    };

    render() {
        return (
            <KeyboardAvoidingView style={styles.container} behavior='padding'>
                <View style={styles.imageContainer}><Image style={styles.image} source={Logo}/></View>

                <Input
                    inputStyle={{color: theme.SECONDARY_COLOR}}
                    autoCorrect={false}
                    placeholder="Nom d'utilisateur"
                    returnKeyType='next'
                    errorMessage={this.state.errors.username}
                    containerStyle={styles.input}
                    autoFocus
                />

                <Input
                    inputStyle={{color: theme.SECONDARY_COLOR}}
                    autoCorrect={false}
                    keyboardType='email-address'
                    placeholder="Adresse email"
                    returnKeyType='next'
                    errorMessage={this.state.errors.email}
                    containerStyle={styles.input}
                />

                <Input
                    inputStyle={{color: theme.SECONDARY_COLOR}}
                    autoCorrect={false}
                    placeholder='Prénom'
                    returnKeyType='next'
                    errorMessage={this.state.errors.first_name}
                    containerStyle={styles.input}
                />

                <Input
                    inputStyle={{color: theme.SECONDARY_COLOR}}
                    autoCorrect={false}
                    placeholder='Nom'
                    returnKeyType='next'
                    errorMessage={this.state.errors.last_name}
                    containerStyle={styles.input}
                />

                <Input
                    inputStyle={{color: theme.SECONDARY_COLOR}}
                    autoCorrect={false}
                    placeholder='Mot de passe'
                    returnKeyType='next'
                    errorMessage={this.state.errors.password}
                    containerStyle={styles.input}
                    secureTextEntry={true}
                />

                <Input
                    inputStyle={{color: theme.SECONDARY_COLOR}}
                    autoCorrect={false}
                    placeholder='Confirmation mot de passe'
                    returnKeyType='go'
                    onSubmitEditing={this._login}
                    errorMessage={this.state.errors.password2}
                    containerStyle={styles.input}
                    secureTextEntry={true}
                />

                <Button title="S'inscrire"
                        containerStyle={styles.input}
                        onPress={this._register}
                        buttonStyle={{backgroundColor: theme.PRIMARY_COLOR}}/>

                <Btn title="Déjà un compte ?" onPress={() => this.props.navigation.navigate('Login')}/>

            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: theme.DEFAULT_PADDING,
    },
    input: {
        paddingVertical: 5,
    },
    imageContainer: {
        width: '100%',
    },
    image: {
        width: '100%',
        resizeMode: 'contain',
    }
});

export default RegisterScreen;
