import React from 'react';
import {View, StyleSheet} from 'react-native';
import ActionSheet from 'react-native-actionsheet';

import {getProducts} from '../services/api';

import ProductsList from '../components/ProductsList';
import ListHeader from '../components/ListHeader';

class ProductsListScreen extends React.Component {
    constructor(props) {
        super(props);

        this.totalPages = 0;
        this.page = 0;

        this.state = {
            products: [],
            loading: true,
            numColumns: 2,
            params: {
                category: this.props.navigation.getParam('idCategory'),
            }
        };
    }

    componentDidMount() {
        this._loadProducts();
    }

    _reset = () => {
        this.totalPages = 0;
        this.page = 0;

        this.setState({
            products: [],
            params: {
                category: this.props.navigation.getParam('idCategory'),
            }
        })
    };

    _changeNumColumns = (nbColumns) => {
        this.setState({numColumns: nbColumns})
    };

    _changeFilters = (filterIndex) => {
        let ordering;
        switch (filterIndex) {
            case 0:
                ordering = 'price_ht';
                break;
            case 1:
                ordering = '-price_ht';
                break;
            case 2:
                ordering = 'title';
                break;
            case 3:
                ordering = '-title';
                break;
            default:
                break;
        }
        this._reset();
        this.setState({params: {...this.state.params, ordering}}, this._loadProducts);
    };

    _showActionSheet = () => {
        this.ActionSheet.show()
    };

    _loadProducts = () => {
        this.setState({loading: true});
        getProducts({...this.state.params, page: this.page + 1})
            .then((resp) => {
                this.page = resp.data.page;
                this.totalPages = resp.data.total_pages;
                this.setState({
                    params: {...this.state.params, page: resp.data.page},
                    products: [...this.state.products, ...resp.data.results]
                })
            })
            .catch((error) => {
                console.log(error.response);
            })
            .finally(() => this.setState({loading: false}));
    };

    render() {
        return (
            <View style={styles.mainContainer}>
                <ProductsList navigation={this.props.navigation}
                              ListHeaderComponent={<ListHeader showActionSheet={this._showActionSheet}
                                                               changeNumColumns={this._changeNumColumns}
                                                               numColumns={this.state.numColumns}/>}
                              products={this.state.products}
                              numColumns={this.state.numColumns}
                              loadProducts={this._loadProducts}
                              page={this.page}
                              totalPages={this.totalPages}/>
                <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={'Choix du filtre :'}
                    options={['Prix Asc', 'Prix Desc', 'Name Asc', 'Name Desc', 'Annuler']}
                    cancelButtonIndex={4}
                    onPress={(index) => this._changeFilters(index)}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    // headerFilter: {
    //     flexDirection: 'row',
    // }
});

export default ProductsListScreen;
