import React from 'react';
import {StyleSheet, KeyboardAvoidingView, View, Button as Btn, Image} from 'react-native';
import {Button, Input} from 'react-native-elements';

import theme from "../styles/theme.style";
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faEnvelope, faKey} from '@fortawesome/free-solid-svg-icons';
import Logo from '../assets/icons/logo.png';

class LoginScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            errors: [],
        }
    }

    _login = () => {
        alert("login");
    };

    render() {
        return (
            <KeyboardAvoidingView behavior='padding' style={styles.container}>
                <View style={styles.imageContainer}><Image style={styles.image} source={Logo}/></View>

                <Input
                    inputStyle={{color: theme.SECONDARY_COLOR}}
                    leftIcon={<FontAwesomeIcon icon={faEnvelope} size={15} color={theme.SECONDARY_COLOR}
                                               style={{marginRight: 10}}/>}
                    autoCorrect={false}
                    keyboardType='email-address'
                    placeholder='Utilisateur ou email'
                    returnKeyType='next'
                    errorMessage={this.state.errors.email}
                    containerStyle={styles.input}
                    autoFocus
                />

                <Input
                    inputStyle={{color: theme.SECONDARY_COLOR}}
                    leftIcon={<FontAwesomeIcon icon={faKey} size={15} color={theme.SECONDARY_COLOR}
                                               style={{marginRight: 10}}/>}
                    autoCorrect={false}
                    placeholder='Mot de passe'
                    returnKeyType='go'
                    onSubmitEditing={this._login}
                    errorMessage={this.state.errors.password}
                    containerStyle={styles.input}
                />

                <Button title="Se connecter"
                        containerStyle={styles.input}
                        onPress={this._login}
                        buttonStyle={{backgroundColor: theme.PRIMARY_COLOR}}/>

                <Btn title="Pas de compte ?" onPress={() => this.props.navigation.navigate('Register')}/>

            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: theme.DEFAULT_PADDING,
    },
    input: {
        paddingVertical: 5,
    },
    imageContainer: {
        width: '100%',
    },
    image: {
        width: '100%',
        resizeMode: 'contain',
    }
});

export default LoginScreen;
