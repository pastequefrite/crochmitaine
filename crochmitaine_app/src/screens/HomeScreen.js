import React from 'react';
import {ScrollView, View, Text, Image, StyleSheet, FlatList} from 'react-native';

import Logo from '../assets/icons/logo.png';
import theme from '../styles/theme.style';

import {getProducts} from '../services/api';
import ProductItem from "../components/ProductItem";
import ListEmptyComponent from "../components/ListEmptyComponent";


class HomeScreen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [],
        };

        this._loadProducts();
    }


    _loadProducts = () => {
        getProducts({ordering: '-created_at', count: 3})
            .then((resp) => {
                this.setState({
                    products: [...this.state.products, ...resp.data.results]
                })
            })
            .catch((error) => {
                console.log(error.response);
            });
    };

    _displayProductDetailsForProduct = (idProduct) => {
        this.props.navigation.navigate('ProductDetails', {idProduct: idProduct});
    };

    render() {
        return (
            <ScrollView style={styles.mainContainer}>
                <View style={styles.imageContainer}>
                    <Image style={styles.image} source={Logo}/>
                </View>
                <View style={styles.newsContainer}>
                    <Text style={styles.title}>Les nouveautés</Text>
                    <FlatList
                        data={this.state.products}
                        renderItem={({item}) => <ProductItem
                            product={item}
                            numColumns={1.5}
                            displayProductDetailsForProduct={this._displayProductDetailsForProduct}
                            displayText='none'
                            imageStyle={{width: 250, height: 250, flex: 0}}
                        />}
                        keyExtractor={item => item.id.toString()}
                        contentContainerStyle={{flexGrow: 1}}
                        ListEmptyComponent={ListEmptyComponent}
                        horizontal
                    />
                </View>
                <View style={styles.aboutContainer}>
                    <Text style={styles.title}>A propos</Text>
                    <Text>Bonjour je m'appelle Winnie l'Ourson (ET OUI COCO LAPINOU DES BOIS). Welcome sur mon application de truc muche mistophe
                    euh j'en christophe toi même tu le connais l'ami du boufeur de pots de miel</Text>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        // flex: 1,
        // justifyContent: 'center',
        paddingHorizontal: theme.DEFAULT_PADDING,
    },
    imageContainer: {
        // flex: 1,
        height: 100,
    },
    title: {
        fontWeight: 'bold',
        fontSize: theme.FONT_SIZE_LARGE,
        textAlign: 'center',
    },
    newsContainer: {
        // flex: 1,
    },
    aboutContainer: {
        // flex: 1,
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
    }
});

export default HomeScreen;
